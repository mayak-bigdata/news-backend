import requests
import json

#BACKEND_PATH = "http://mayak.app"
BACKEND_PATH = "http://35.244.212.175"

if __name__ == '__main__':

#########################
    print("### Sending a feedback ###")

    print("Logging in user...")

    headers = {
        'Content-Type': 'application/json'
    }
    data = {
        'email': 'user@gmail.com',
        'password': '123456'
    }

    url = BACKEND_PATH + '/registration/login'

    resp = requests.post( url = url, data = json.dumps(data), headers=headers)

    print(resp.text)

    url = BACKEND_PATH + '/recommender/feedback'

    print('Sending feedback')

    token = resp.json()['auth_token']

    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }

    reactions = {'reactions': {"11":1, "8":1}}

    resp = requests.post( url = url, data = json.dumps(reactions), headers = headers )

    print(resp.text)

#########################
    print("### Getting feedback ###")

    url = BACKEND_PATH + '/recommender/feedback'

    resp = requests.get(url, headers = headers)

    print(resp.text)

#########################
    print("Getting recommendations")

    url = BACKEND_PATH + '/recommender/recommend'

    resp = requests.get( url = url, headers = headers )

    print(resp.text)
