import requests
import json
import codecs

#BACKEND_PATH = "http://mayak.app"
#BACKEND_PATH = "http://35.244.212.175"
BACKEND_PATH = "http://localhost:8000"

if __name__ == '__main__':

    headers = {
        'Content-Type': 'application/json'
    }
    new_cat = {
      "name": "Культура"
    }

    # Creating a category
    url = BACKEND_PATH + '/admin/category/'

    resp = requests.post(url = url, data = json.dumps(new_cat, ensure_ascii=False).encode('utf-8'), headers=headers)

    print(resp.json())

    # Create news

    url = BACKEND_PATH + '/admin/news/'

    new_news = {
        "cat": 0,
        "text": "This is a sample news title",
        "name": "Super puper news"
    }

    resp = requests.post(url = url, data = json.dumps(new_news, ensure_ascii=False).encode('utf-8'), headers=headers)

    print(resp.json())

    # Get news

    url = BACKEND_PATH + '/admin/news/?num=2'

    query = {
        "num": 4
    }

    resp = requests.get(url = url, data = json.dumps(new_news, ensure_ascii=False).encode('utf-8'), headers=headers)

    print(resp.json())

    # Upload file

    url = BACKEND_PATH + '/admin/active/'

    headers = {
        'Content-Type': 'multipart/form-data'
    }
    query = {
        'news_id': 1,
        'audio_name': 'bts.mp3',
    }

    files = {'file': open('bts.mp3', 'rb')}

    resp = requests.post( url, data=query, files=files )

    print(resp.json())

    # Test digests

    headers = {
        'Content-Type': 'application/json'
    }
    new_cat = {
      "name": "Политика",
      "contents": [1]
    }

    url = BACKEND_PATH + '/admin/digest/'

    resp = requests.post( url, data=json.dumps(new_cat), headers=headers )

    print(resp.json())

    url = BACKEND_PATH + '/admin/digest/Политика'

    resp = requests.delete( url, headers=headers)

    print(resp.json())
