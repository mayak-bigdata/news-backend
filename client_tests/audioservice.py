import requests
import json

#BACKEND_PATH = "http://mayak.app"
BACKEND_PATH = "http://35.244.212.175"

if __name__ == '__main__':

#########################
    print("### Getting a digest URL ###")

    print("Logging in user...")

    headers = {
        'Content-Type': 'application/json'
    }
    data = {
        'email': 'econpolit@gmail.com',
        'password': '123456'
    }

    url = BACKEND_PATH + '/registration/login'

    resp = requests.post( url = url, data = json.dumps(data), headers=headers)

    print(resp.text)

    token = resp.json()['auth_token']

    print("Getting a URL...")

    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }

    url = BACKEND_PATH + "/digests/get/Экономика"

    resp = requests.get( url = url, headers = headers )

    print(resp.text)


#########################

    print("### Get description of digests ###")

    print("Logging in user...")

    headers = {
        'Content-Type': 'application/json'
    }
    data = {
        'email': 'user@gmail.com',
        'password': '123456'
    }

    url = BACKEND_PATH + '/registration/login'

    resp = requests.post( url = url, data = json.dumps(data), headers=headers)

    print(resp.text)

    token = resp.json()['auth_token']

    print("Getting a list of digests...")

    url = BACKEND_PATH + '/digests/list'

    digests = requests.get( url = url, headers=headers)
    print(digests.json())

    print("Getting summaries...")

    url = BACKEND_PATH + '/digests/summary/'
    headers = headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }

    for name in digests.json():

        resp = requests.get( url = url + name, headers = headers )
        print("DIGEST")
        print(resp.text)
