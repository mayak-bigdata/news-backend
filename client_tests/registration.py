import requests
import json

#BACKEND_PATH = "http://mayak.app"
BACKEND_PATH = "http://35.244.212.175"

if __name__ == "__main__":

    #####
    # Register user
    #####

    print("Registering user...")

    headers = {
        'Content-Type': 'application/json'
    }
    data = {
        'email': 'econpolit@gmail.com',
        'password': '123456',
        'preferences': '["RussiaToday", "Lentach"]',
    }

    url = BACKEND_PATH + '/registration/register'

    resp = requests.post( url = url, data = json.dumps(data), headers=headers)

    print("Server answer: ", resp.text)

    # Success answer:
    {
     "auth_token":"yourtoken",
     "message":"Successfully registered.",
     "status":"success"
    }

    # Fail answer:
    {"message":"User already exists. Please Log in.","status":"fail"}

    #####
    # Login user
    #####

    print("Logging in user...")

    headers = {
        'Content-Type': 'application/json'
    }
    data = {
        'email': 'user@gmail.com',
        'password': '123456'
    }

    url = BACKEND_PATH + '/registration/login'

    resp = requests.post( url = url, data = json.dumps(data), headers=headers)

    token = resp.json()['auth_token']
    # Success answer
    {"auth_token":"yourtoken",
    "message":"Successfully logged in.",
    "status":"success"}

    print("Server answer: ", resp.text)

    #####
    # User info
    #####

    print("Getting user info...")

    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }

    url = BACKEND_PATH + '/registration/info'

    resp = requests.get( url = url, headers=headers )

    print("Server answer: ", resp.text)
