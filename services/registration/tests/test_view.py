# registration/tests/test_view.py

import unittest
import json
from helpers import sample_user, format_title

from src import db
from src.models import User
from tests.base import BaseTestCase

class TestRegistrationBlueprint(BaseTestCase):


    ### Registration tests


    def test_registration(self):
        format_title("Test for user registration")
        with self.client:
            print("Registrating user...")
            response = self.client.post(
                '/registration/register',
                data=json.dumps(sample_user()),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            print(data)
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully registered.')
            print("Register successfull. Your token:")
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 201)
            print(data['auth_token'])

    def test_registered_with_already_registered_user(self):
        format_title("Test registration with already registered email")
        print("Running already registered user scenario")
        user = User(
            **sample_user()
        )
        db.session.add(user)
        db.session.commit()
        with self.client:
            response = self.client.post(
                '/registration/register',
                data=json.dumps(dict(
                    **sample_user()
                )),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(
                data['message'] == 'User already exists. Please Log in.')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 202)
        print("Everything went fine!")


    ### Login tests


    def test_registered_user_login(self):
        format_title("Test for login of registered-user login")
        with self.client:
            # user registration
            print("\nRegistrating user...")
            resp_register = self.client.post(
                '/registration/register',
                data=json.dumps(sample_user()),
                content_type='application/json',
            )
            data_register = json.loads(resp_register.data.decode())
            self.assertTrue(data_register['status'] == 'success')
            self.assertTrue(
                data_register['message'] == 'Successfully registered.'
            )
            self.assertTrue(data_register['auth_token'])
            self.assertTrue(resp_register.content_type == 'application/json')
            self.assertEqual(resp_register.status_code, 201)
            print("User registered!")
            print("Logging in...")
            # registered user login
            response = self.client.post(
                '/registration/login',
                data=json.dumps(sample_user()),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully logged in.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)
            print("User logged in!")
            print(data)


    def test_wrong_password(self):
        format_title("Test for login with wrong password")
        with self.client:
            # user registration
            print("\nRegistrating user...")
            resp_register = self.client.post(
                '/registration/register',
                data=json.dumps(sample_user()),
                content_type='application/json',
            )
            data_register = json.loads(resp_register.data.decode())
            self.assertTrue(data_register['status'] == 'success')
            self.assertTrue(
                data_register['message'] == 'Successfully registered.'
            )
            self.assertTrue(data_register['auth_token'])
            self.assertTrue(resp_register.content_type == 'application/json')
            self.assertEqual(resp_register.status_code, 201)
            print("User registered!")
            print("Logging in...")
            # registered user login
            response = self.client.post(
                '/registration/login',
                data=json.dumps(sample_user(def_password='wrongpass')),
                content_type='application/json'
            )
            data = json.loads(response.data.decode())
            print(data)
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(data['message'] == 'Wrong password. Try again')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 403)
            print("We are not logged in!")
            print(data)

    ### Get user data

    def test_user_status(self):

        format_title("Test for user status")

        with self.client:

            print('Registrating user...')
            resp_register = self.client.post(
                '/registration/register',
                data=json.dumps(sample_user()),
                content_type='application/json'
            )

            response = self.client.get(
                '/registration/info',
                headers=dict(
                    Authorization='Bearer ' + json.loads(
                        resp_register.data.decode()
                    )['auth_token']
                )
            )
            print('Registered.')
            print('User data: ')
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['data'] is not None)
            self.assertEqual(response.status_code, 200)
            print(data)

if __name__ == '__main__':
    unittest.main()
