# registration/tests/test_user_model.py

import unittest

from helpers import sample_user, format_title

from src import db
from src.models import User
from tests.base import BaseTestCase

class TestUserModel(BaseTestCase):

    def test_encode_auth_token(self):

        format_title("Test token encoding")
        print("Creating user...")
        user = User(
            **sample_user()
        )
        db.session.add(user)
        db.session.commit()
        print("User created...\nCreating token...")
        auth_token = user.encode_auth_token(user.id)
        print(auth_token)
        self.assertTrue(isinstance(auth_token, bytes))
        print("Token is ok!")

    def test_decode_auth_token(self):
        format_title("Test token decoding")
        user = User(
            **sample_user()
        )
        db.session.add(user)
        db.session.commit()
        print("User created")
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))
        print("Token created")
        self.assertTrue(User.decode_auth_token(auth_token) == 1)
        print("Decoding works!")

if __name__ == '__main__':
    unittest.main()
