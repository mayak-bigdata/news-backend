#!/bin/sh

python manage.py create_db
python manage.py db init
python manage.py db migrate
gunicorn -b 0.0.0.0:5060 manage:app 
