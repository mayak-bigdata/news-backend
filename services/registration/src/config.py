# registration/src/config.py

import os
import datetime

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    # DataBase config
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Tokens config
    TOKEN_LIFE = datetime.timedelta(hours = int(os.environ.get('TOKEN_LIFE')))
    TOKEN_KEY  = os.environ.get('TOKEN_KEY')

    # Encryption
    BCRYPT_LOG_ROUNDS = int(os.environ.get('BCRYPT_LOG_ROUNDS'))
