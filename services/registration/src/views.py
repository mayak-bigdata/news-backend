# registration/src/views.py

from flask import Blueprint, request, make_response, jsonify
from flask.views import MethodView

from src import bcrypt, db
from src.models import User

registration_blueprint = Blueprint('registration', __name__)

#########
### Healthcheck
#########

class HealthCheck(MethodView):

    def get(self):
        responseObject = {
            'message': 'ok'
        }
        return make_response(jsonify(responseObject)), 200

#########
### Registration handler
#########

class RegisterAPI(MethodView):
    """
    User Registration Resource
    """

    def post(self):
        # get the post data
        post_data = request.get_json()
        # check if user already exists
        user = User.query.filter_by(email=post_data.get('email')).first()
        if not user:
            try:
                user = User(
                    email=post_data.get('email'),
                    password=post_data.get('password'),
                    preferences=post_data.get('preferences')
                )

                # insert the user
                db.session.add(user)
                db.session.commit()
                # generate the auth token
                auth_token = user.encode_auth_token(user.id)
                responseObject = {
                    'status': 'success',
                    'message': 'Successfully registered.',
                    'auth_token': auth_token.decode()
                }
                return make_response(jsonify(responseObject)), 201
            except Exception as e:
                responseObject = {
                    'status': 'fail',
                    'message': 'Some error occurred. Please try again.'
                }
                return make_response(jsonify(responseObject)), 401
        else:
            responseObject = {
                'status': 'fail',
                'message': 'User already exists. Please Log in.',
            }
            return make_response(jsonify(responseObject)), 202

#########
### Login handler
#########


class LoginAPI(MethodView):
    """
    User Login Resource
    """
    def post(self):
        # get the post data
        post_data = request.get_json()
        try:
            # fetch the user data
            user = User.query.filter_by(
                email=post_data.get('email')
            ).first()

            if user:

                if bcrypt.check_password_hash( user.password, post_data.get('password') ):
                    auth_token = user.encode_auth_token(user.id)
                    if auth_token:
                        responseObject = {
                            'status': 'success',
                            'message': 'Successfully logged in.',
                            'auth_token': auth_token.decode()
                        }
                        return make_response(jsonify(responseObject)), 200
                else:
                    responseObject = {
                        'status': 'fail',
                        'message': 'Wrong password. Try again'
                    }
                    return make_response(jsonify(responseObject)), 403

            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'User does not exist.'
                }
                return make_response(jsonify(responseObject)), 404

        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Something went wrong!',
            }
            return make_response(jsonify(responseObject)), 500

#########
### User information
#########


class UserAPI(MethodView):
    """
    User Resource
    """
    def get(self):
        # get the auth token
        try:
            auth_header = request.headers.get('Authorization')
            if auth_header:
                auth_token = auth_header.split(" ")[1]
            else:
                auth_token = ''
            if auth_token:
                resp = User.decode_auth_token(auth_token)
                if not isinstance(resp, str):
                    user = User.query.filter_by(id=resp).first()
                    responseObject = {
                        'status': 'success',
                        'data': {
                            'user_id': user.id,
                            'email': user.email,
                            'registered_on': user.registered_on
                        }
                    }
                    return make_response(jsonify(responseObject)), 200
                responseObject = {
                    'status': 'fail',
                    'message': resp
                }
                return make_response(jsonify(responseObject)), 401
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Provide a valid auth token.'
                }
                return make_response(jsonify(responseObject)), 401
        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Something went wrong.',
                'exc': e
            }
            return make_response(jsonify(responseObject)), 500


###########
### Miscelanious
###########

# define the API resources
registration_view = RegisterAPI.as_view('register_api')
login_view = LoginAPI.as_view('login_api')
info_view = UserAPI.as_view('info_api')
health_view = HealthCheck.as_view('health_api')


# add Rules for API Endpoints
registration_blueprint.add_url_rule(
    '/registration/register',
    view_func=registration_view,
    methods=['POST']
)

registration_blueprint.add_url_rule(
    '/registration/login',
    view_func=login_view,
    methods=['POST']
)

registration_blueprint.add_url_rule(
    '/registration/info',
    view_func=info_view,
    methods=['GET']
)

registration_blueprint.add_url_rule(
    '/',
    view_func=health_view,
    methods=['GET']
)
