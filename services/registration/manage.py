# registration/manage.py

from src import app, db, models

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

import unittest

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

@manager.command
def create_db():
    """Creates the db tables."""
    db.create_all()

@manager.command
def drop_db():
    """Drops the db tables."""
    db.drop_all()

@manager.command
def test():
    """Runs unit tests."""
    tests = unittest.TestLoader().discover('tests/', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1

@manager.command
def test_run():
    app.run(host='0.0.0.0', debug=True, port=8000)

if __name__ == '__main__':
    manager.run()
