# audioservice/tests/base.py


from flask_testing import TestCase

from src import app, config


class BaseTestCase(TestCase):
    """ Base Tests """

    def create_app(self):
        app.config.from_object(config.Config())
        return app
