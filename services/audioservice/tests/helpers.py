# audioservice/tests/helpers.py

def sample_user(def_email='test@test.com', def_password='test', def_preferences='["RT", "Lentach"]'):
    return dict(
        email=def_email,
        password=def_password,
        preferences=def_preferences
    )

def format_title(title):
    print("""\n====={}=====\n""".format(title))
