# registration/tests/test_view.py

import unittest
import json
from helpers import sample_user, format_title

from tests.base import BaseTestCase

class TestAudioserviceBlueprint(BaseTestCase):

    ### Registration tests

    def test_listing(self):
        format_title("Testing if authentication and listing works")
        with self.client:

            response = self.client.get(
                '/digests/list',
                content_type='application/json'
            )

            data = json.loads(response.data.decode())

            print("Got list successfully:")
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)
            print(data)


if __name__ == '__main__':
    unittest.main()
