# audioservice/manage.py

from src import app

from flask_script import Manager
import unittest

manager = Manager(app)

@manager.command
def test():
    """Runs unit tests."""
    tests = unittest.TestLoader().discover('tests/', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1
    
@manager.command
def test_run():
    app.run(host='0.0.0.0', debug=True, port=8000)

if __name__ == '__main__':
    manager.run()
