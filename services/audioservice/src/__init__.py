# audioservice/src/__init__.py

import os
from flask import Flask
from .config import Config
from .db import DB
from .concatenator import Concatenator

app = Flask(__name__)
app.config.from_object(Config)


conf = Config()
db = DB( conf.PARTS_PATH, conf.HLS_PATH, conf.SYNC_URL )

concat = Concatenator(conf.PARTS_PATH, conf.HLS_PATH)

from .views import audioserice_blueprint
app.register_blueprint(audioserice_blueprint)
