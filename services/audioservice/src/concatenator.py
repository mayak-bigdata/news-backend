import subprocess
import sys
import os

#TODO: Make this shit a pip package

class Concatenator:

    def __init__(self, parts_path, hls_path):
        self.parts_path = parts_path
        self.hls_path = hls_path

    def _prepare_hls(self, input_file, digest_name, hls_path):

        dig_path = os.path.join(hls_path, digest_name)
        os.makedirs(dig_path, exist_ok=True)

        command = "ffmpeg \
          -i {} \
          -vn -ac 2 -acodec aac \
          -start_number 0 -hls_time 10 -hls_list_size 0 -f hls {}".format(input_file, os.path.join(dig_path, 'index.m3u8'))

        return subprocess.run(command.split(), check=True)

    def _concat_mp3(self, file_list, input_dir, output_dir, output_name):

        input_files = map(lambda x: os.path.join(input_dir, x), file_list)
        output_path = os.path.join(output_dir, output_name)
        list_name = 'list_{}.txt'.format(output_name)

        with open(list_name, 'w+') as f:
            f.write('\n'.join(map(lambda x: 'file ' + "'{}'".format(x), input_files)))

        command = 'ffmpeg -f concat -safe 0 -i {} -acodec copy {}'.format(list_name, output_path)

        code = subprocess.run(command.split(), check=True)
        os.remove(list_name)

        return code

    def prepare_digest(self, file_list: list, digest_name: str):

        temp_name = digest_name+'.mp3'
        self._concat_mp3( file_list, self.parts_path, '.', temp_name)
        self._prepare_hls( temp_name, digest_name, self.hls_path)
        os.remove(temp_name)

        return 0
