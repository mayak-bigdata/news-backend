# audioservice/src/views.py

from flask import Blueprint, request, make_response, jsonify, redirect
from flask.views import MethodView
from src import db, app, concat
import requests
import json
import os

audioserice_blueprint = Blueprint('audioservice', __name__)

#TODO: Build normal logging

#########
# Getting metainformation about digests
#########


class ListAPI(MethodView):
    """
    Listing digests resource
    """

    def get(self):

        names = list(db.digests.keys())

        try:

            resp = requests.get(app.config.get('REC_URL'), timeout=5)
            #resp = requests.get(app.config.get('LOGIN_URL'), timeout=5)

            if resp.status_code == 200:
                names.append('Personal')

        except requests.exceptions.RequestException as e:
            print('Personalizer is not working!')

        return make_response(jsonify(names)), 200


#########
# Get a link for a digest
#########

class DownloadAPI(MethodView):
    """
    Get a link for a digest
    """

    def get(self, digest_name):

        try:

            auth_header = request.headers

            ## Get ids of the news in the digest

            if digest_name in db.digests.keys():
                ids = db.digests[digest_name]

            elif digest_name == 'Personal':
                rec_url = '{}/recommender/recommend'.format(app.config.get('REC_URL'))
                resp = requests.get(rec_url, headers=auth_header)

                if resp.status_code != 200:
                    return (resp.text, resp.status_code, resp.headers.items())

                ids = resp.json()["rec"]
                user_id = resp.json()["user_id"] # so that we don't have to make another
                                                 # request to the id service

            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Resource not found.'
                }
                return make_response(jsonify(responseObject)), 404


            ## Send blank reactions to mark news being viewed

            reactions = {'reactions':{ x:0 for x in ids }}
            react_url = '{}/recommender/feedback'.format(app.config.get('REC_URL'))

            try:
                resp = requests.post( url = react_url, data = json.dumps(reactions),
                                      headers = auth_header, timeout = 5 )
                if resp.status_code != 200:
                    if digest_name == 'Personal':
                        return (resp.text, resp.status_code, resp.headers.items())

            except requests.exceptions.RequestException as e:
                print('Personalizer does not work!')


            ## Give url for a digest and build audio for Personal one

            if digest_name == 'Personal':

                # Here audio concatenation happens

                digest_name = str(user_id)

                if digest_name not in os.listdir( app.config.get('HLS_PATH')):
                    files = [db.parts[x] for x in ids]
                    concat.prepare_digest(files, digest_name)

            digest_url = 'http://' + app.config.get('EXTERNAL_URL') + '/digests/hls/' + digest_name + '/index.m3u8'

            responseObject = {
                'status': 'success',
                'url': digest_url
            }

            return make_response(jsonify(responseObject)), 200

        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Something went wrong.',
                'error': e
            }
            return make_response(jsonify(responseObject)), 500

#########
# Get a digest summary
#########

class SummaryAPI(MethodView):
    """
    Get summary of news in the digest
    """

    def get(self, digest_name):

        try:

            headers = request.headers

            if digest_name in db.digests.keys():

                contents = db.digests[digest_name]

            elif digest_name == 'Personal':

                rec_url = "{}/recommender/recommend".format(app.config.get('REC_URL'))

                try:
                    resp = requests.get(rec_url, headers=headers)

                    if resp.status_code != 200:
                        return (resp.text, resp.status_code, resp.headers.items())

                    contents = resp.json()["rec"]

                except requests.exceptions.RequestException as e:
                    print(e)
                    responseObject = {
                        'status': 'fail',
                        'message': 'Personalization is currently unavailable.'
                    }
                    return make_response(jsonify(responseObject)), 503

            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Resource not found.'
                }
                return make_response(jsonify(responseObject)), 404

            responseObject = {
                'status': 'success',
                'name': digest_name,
                'contents': [db.news[x] for x in contents]
            }
            return make_response(jsonify(responseObject)), 200


        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Something went wrong.',
                'error': e
            }
            return make_response(jsonify(responseObject)), 500

#########
# Synchronise state
#########

class SyncAPI(MethodView):
    """
    Sync state with admin server
    """

    def post(self):

        try:
            db.sync()
        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Internal error'
            }
            return make_response(jsonify(responseObject)), 500

        responseObject = {
            'status': 'success'
        }
        return make_response(jsonify(responseObject)), 200

#########
# Infrastructure stuff
#########

# functions
list_view = ListAPI.as_view('list_api')
download_view = DownloadAPI.as_view('download_api')
summary_view = SummaryAPI.as_view('summary_api')
sync_view = SyncAPI.as_view('sync_api')

# making handlers
audioserice_blueprint.add_url_rule(
    '/digests/list',
    view_func=list_view,
    methods=['GET']
)
audioserice_blueprint.add_url_rule(
    '/digests/get/<digest_name>',
    view_func=download_view,
    methods=['GET']
)
audioserice_blueprint.add_url_rule(
    '/digests/summary/<digest_name>',
    view_func=summary_view,
    methods=['GET']
)
audioserice_blueprint.add_url_rule(
    '/digests/sync',
    view_func=sync_view,
    methods=['POST']
)
