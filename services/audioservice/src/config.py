# audioservice/src/config.py

import os

class Config(object):

    DATA_PATH  = os.environ.get('DATA_PATH')
    PARTS_PATH = os.path.join(DATA_PATH, 'parts')
    HLS_PATH   = os.path.join(DATA_PATH, 'digests/hls')

    LOGIN_IP   = os.environ.get('REGISTRATION_SERVICE_HOST')
    LOGIN_PORT = os.environ.get('REGISTRATION_SERVICE_PORT')
    LOGIN_URL  = "http://{}:{}".format(LOGIN_IP, LOGIN_PORT)

    REC_IP     = os.environ.get('RECOMMENDER_SERVICE_HOST')
    REC_PORT   = os.environ.get('RECOMMENDER_SERVICE_PORT')
    REC_URL    = "http://{}:{}".format(REC_IP, REC_PORT)

    SYNC_IP    = os.environ.get('ADMIN_SERVICE_HOST')
    SYNC_PORT  = os.environ.get('ADMIN_SERVICE_PORT')
    SYNC_URL   = "http://{}:{}".format(SYNC_IP, SYNC_PORT) + '/admin'

    EXTERNAL_URL = os.environ.get('EXTERNAL_URL')


    JSON_AS_ASCII = False
