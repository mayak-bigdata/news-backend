import json
import requests
import subprocess
import os
import glob
import shutil


## File utils

def clean_directory(path):
    files = glob.glob(os.path.join(path, '*'))
    for f in files:
        if os.path.isfile(f):
            os.remove(f)

def move_files(srcDir, dstDir):
    if os.path.isdir(srcDir) and os.path.isdir(dstDir) :
        for filePath in glob.glob(os.path.join(srcDir, '*')):
            shutil.move(filePath, dstDir);
    else:
        raise Exception('Not directories: {}, {}, {}'.format(srcDir, dstDir, os.getcwd()))


class DB:
    def __init__(self, parts_path, hls_path, sync_url):

        # Download data from state service
        self.parts_path = parts_path
        self.hls_path   = hls_path
        self.sync_url   = sync_url

        self.parts = {}
        self.digests = {}
        self.news = {}

        self.sync()

    def sync(self):

        headers = {
            'Content-Type': 'application/json'
        }

        data = requests.get(url = self.sync_url + '/sync')
        file = requests.get(url = self.sync_url + '/sync/static')

        if data.status_code != 200 or file.status_code != 200:
            print(data.text)
            print(file.text)
            raise Exception('No response')

        data = data.json()

        arch_name = 'file.tar.gz'
        with open(arch_name, 'wb+') as f:
            f.write(file.content)

        subprocess.check_call(['tar', 'xf', arch_name])


        parts   = {p['id']: p['audio_name'] for x in data for p in x['contents']}
        digests = {x['name']: [y['id'] for y in x['contents']] for x in data}
        news    = {p['id']: {'name': p['name'], 'description': p['text']}
                                          for x in data for p in x['contents']}
        ## Move files

        clean_directory(self.parts_path)
        move_files('digests/parts', self.parts_path)

        for dir in glob.glob(os.path.join(self.hls_path, '*')):
            shutil.rmtree(dir)
        move_files('digests/hls', self.hls_path)

        self.parts = parts
        self.digests = digests
        self.news = news

        return 0
        """
        with open(path) as json_file:
            data = json.load(json_file)
            self.parts = {x['id']:x['file_name'] for x in data['contents']}
            self.digests = {x['name']: x['contents'] for x in data['digests']}
            self.news = {x['id']: x for x in data['contents']}
        """
