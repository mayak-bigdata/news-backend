flask==1.0.2
flask_script==2.0.6
flask_testing==0.7.1
gunicorn==19.8.1
requests
python-telegram-bot>=2.5
