# bot/src/config.py

import os

class Config(object):

    TARGET_DIGEST = os.environ.get('TARGET_DIGEST')

    AUDIO_IP   = os.environ.get('AUDIOSERVICE_SERVICE_HOST')
    AUDIO_PORT = os.environ.get('AUDIOSERVICE_SERVICE_PORT')
    AUDIO_URL    = "http://{}:{}".format(AUDIO_IP, AUDIO_PORT)

    LOGIN_IP   = os.environ.get('REGISTRATION_SERVICE_HOST')
    LOGIN_PORT = os.environ.get('REGISTRATION_SERVICE_PORT')
    LOGIN_URL  = "http://{}:{}".format(LOGIN_IP, LOGIN_PORT)

    USER_NAME = os.environ.get('USER_NAME')
    USER_PASS = os.environ.get('USER_PASS')

    CHANNEL_NAME = os.environ.get('CHANNEL_NAME')
    BOT_KEY      = os.environ.get('BOT_KEY')
    JSON_AS_ASCII = False
