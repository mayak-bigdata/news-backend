#bot/src/views.py

import sys
import os
import telegram
from flask import Flask, request
import requests
from datetime import datetime
import json
import subprocess

from .config import Config

app = Flask(__name__)
app.config.from_object(Config)

bot = telegram.Bot(token=app.config.get('BOT_KEY'))

@app.route('/bot/post_latest', methods=['GET'])
def post_latest():
    if request.method == "GET":

        # Authenticate

        headers = {
            'Content-Type': 'application/json'
        }
        data = {
            'email': app.config.get('USER_NAME'),
            'password': app.config.get('USER_PASS')
        }

        url = app.config.get('LOGIN_URL') + '/registration/login'

        resp = requests.post( url = url, data = json.dumps(data), headers=headers)

        token = resp.json()['auth_token']

        auth_headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        # Get most recent news

        resp = requests.get("{}/digests/summary/{}".format( app.config.get('AUDIO_URL'),
                                                            app.config.get('TARGET_DIGEST')))
        contents = resp.json()['contents']

        parts = ["* {} *\n{}".format(x['name'], x['description']) for x in contents]
        message = '\n\n'.join(parts)

        date = datetime.now().strftime('%d.%m.%Y')
        message = '* Дайджест от {} *\n\n'.format(date) + message + '\n'

        # Get URL

        resp = requests.get("{}/digests/get/{}".format( app.config.get('AUDIO_URL'),
                                                            app.config.get('TARGET_DIGEST')), headers=auth_headers)
        url = resp.json()['url']

        # This happens because telegram does not support m3u8

        command = 'ffmpeg -i {}/digests/hls/{}/index.m3u8 -codec copy file.mp4'.format(app.config.get('AUDIO_URL'),
                                                                                         app.config.get('TARGET_DIGEST'))
        subprocess.run(command.split(), check=True)

        # repeat the same message back (echo)
        cn = app.config.get('CHANNEL_NAME')
        bot.sendMessage(chat_id=cn, text=message,  parse_mode=telegram.ParseMode.MARKDOWN)
        bot.sendAudio(chat_id=cn, audio=open('file.mp4', 'rb'), disable_notification=True)

        os.remove('file.mp4')

    return 'ok'

@app.route('/')
def index():
    return 'ok', 200
