from flask import Flask, jsonify, request
from flask_restx import Api
from flask_injector import FlaskInjector
from pymongo import MongoClient
import pymongo

from src.routes import register_routes
from src.config import Config

# Statefull controllers

from src.category.service import CategoryService
from src.news_title.service import NewsTitleService
from src.active_title.service import ActiveTitleService
from src.digest.service import DigestService
from src.sync.service import SyncService

# App Declaration

def create_app(env=None):

    # Declaration

    app = Flask(__name__)
    app.config.from_object(Config())

    api = Api(app, title="Administration pannel", version="0.1.0")

    register_routes(api, app, root='admin')

    @app.route("/")
    def health():
        return jsonify("ok")

    # Resource injection

    cl = MongoClient('mongodb+srv://recommender:pass@mayakreact-bbx0n.gcp.mongodb.net/recommender_db?retryWrites=true&w=majority')
    db = cl['recommender_db']

    cat_service  = (CategoryService, CategoryService(db))
    news_service = (NewsTitleService, NewsTitleService(db, cat_service[1]))
    act_service  = (ActiveTitleService, ActiveTitleService(news_service[1], save_path=app.config.get('SAVE_PATH')))
    digest_service = (DigestService, DigestService(act_service[1]))
    sync_service = (SyncService, SyncService(
                                     cache_path=app.config.get('STATE_PATH'),
                                     digest_serv=digest_service[1],
                                     active_serv=act_service[1],
                                     news_serv=news_service[1],
                                     sync_paths=[app.config.get('AUDIO_URL')]
                   ))

    services = [cat_service, news_service, act_service, digest_service, sync_service]

    def bind_service(service):
        def bind(binder):
            binder.bind(
                service[0],
                to=service[1]
            )
        return bind

    FlaskInjector(
        app=app,
        modules=[bind_service(x) for x in services]
        #modules=[cat, news, act, digest]
    )

    return app
