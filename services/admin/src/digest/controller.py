from flask import request, abort, jsonify
from flask_accepts import accepts, responds
from flask_restx import Namespace, Resource
from flask.wrappers import Response
from typing import List, Dict
from injector import inject

from .object import Digest, DigestSchema, DigestInterface

from .service import DigestService, NoActive

api = Namespace('Digest', description='Responsible for combining active news into digests.')

######
## Digest resources
######

@api.route('/')
class DigestResource(Resource):
    '''Digest'''

    @inject
    def __init__(self, service: DigestService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    @responds(schema=DigestSchema(many=True), api=api)
    def get(self) -> List[Digest]:
        '''Get list of available digests'''
        return self.service.get_available()

    @accepts(schema=DigestSchema, api=api)
    @responds(schema=DigestSchema, api=api)
    def post(self) -> Digest:
        """Create a new digest from news with static data."""
        try:

            return self.service.insert(request.parsed_obj)

        except NoActive as exep:
            abort(404, description='Title with name: {} is not active'.format(exep.name))


### Control of specifics digests

@api.route('/<string:digestName>')
@api.param('digestName', "You won't guess")
class DigestIdResource(Resource):

    @inject
    def __init__(self, service: DigestService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    @responds(schema=DigestSchema, api=api)
    def get(self, digestName: str) -> Digest:
        '''Get a digest'''

        try:
            return self.service.get(digestName)
        except KeyError:
            abort(404, description='Data was not found.')

    def delete(self, digestName: str) -> Dict[str, str]:
        '''Delete a digest'''

        if not self.service.has(digestName):
            abort(404, description="Digest doesn't exist")

        name = self.service.delete(digestName)

        return dict(status='ok', name=name)
