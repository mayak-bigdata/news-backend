from mypy_extensions import TypedDict
from marshmallow import fields, Schema
from dataclasses import dataclass
from typing import List

### Model

@dataclass
class Digest:

    name: str
    contents: List[int]

### Interface

class DigestInterface(TypedDict, total=False):

    name: str
    contents: List[int]

### Schema

class DigestSchema(Schema):

    name = fields.String(required=True, description='Name of the digest')
    contents = fields.List(fields.Integer(), required=True, description='Ids of active titles that you want to include in a digest')
