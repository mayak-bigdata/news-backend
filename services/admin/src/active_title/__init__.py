# src/category/__init__.py

BASE_ROUTE = 'active'

def register_routes(api, app, root='api'):
    from .controller import api as active_api

    api.add_namespace(active_api, path=f'/{root}/{BASE_ROUTE}')
