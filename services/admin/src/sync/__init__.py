# src/news_title/__init__.py

BASE_ROUTE = 'sync'

def register_routes(api, app, root='api'):
    from .controller import api as sync_api

    api.add_namespace(sync_api, path=f'/{root}/{BASE_ROUTE}')
