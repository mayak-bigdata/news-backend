import os
import shutil
import subprocess
from typing import List
import requests


from src.digest.service import DigestService
from src.active_title.service import ActiveTitleService
from src.news_title.service import NewsTitleService

import src.sync.audio as audio

class SyncService():

    def __init__(
        self,
        cache_path:  str,
        digest_serv: DigestService,
        active_serv: ActiveTitleService,
        news_serv:   NewsTitleService,
        sync_paths: List[str]
        ):
        self.cache_path  = cache_path
        self.digest_serv = digest_serv
        self.active_serv = active_serv
        self.news_serv   = news_serv
        self.sync_paths  = sync_paths
        self.state = []

        # TODO: Here you must delete what has been left over from the last time.


    def update_state(self):

        # Create digests dictionary

        def insert_fields(news_id):
            files = self.active_serv.get(news_id)
            meta  = self.news_serv.get(news_id)
            return {
                'id': news_id,
                'name': meta.name,
                'text': meta.text,
                'audio_name': files.audio_name,
                'cat': meta.cat
            }

        insert_news = lambda x: {'name':x.name, 'contents':[insert_fields(x) for x in x.contents]}
        digests = list(map(insert_news, self.digest_serv.get_available()))

        # Create an archive with audio files

        if os.path.isdir( self.cache_path ):
            shutil.rmtree( self.cache_path )

        parts_path = os.path.join( self.cache_path, 'parts' )
        hls_path   = os.path.join( self.cache_path, 'hls')
        os.makedirs( self.cache_path )
        os.makedirs( parts_path )
        os.makedirs( hls_path )

        for dig in digests:

            for x in dig['contents']:
                orig_path = os.path.join( self.active_serv.save_path, x['audio_name'] )
                shutil.copy( orig_path, os.path.join(parts_path, x['audio_name']) )

            audio.concat_mp3(
                file_list = [ x['audio_name'] for x in dig['contents'] ],
                input_dir = parts_path,
                output_dir = '.',
                output_name = 'temp.mp3'
            )
            audio.prepare_hls(
                input_file = 'temp.mp3',
                digest_name = dig['name'],
                hls_path = hls_path
            )
            os.remove('list.txt')
            os.remove('temp.mp3')

        # Creation itself

        archive_path = 'digests.tar.gz'
        subprocess.check_call(['tar', '-czf', archive_path, self.cache_path])

        self.state = digests
        self.archive_path = os.path.abspath(archive_path)
        self.archive_name = archive_path

        self.sync_state()

        return 0

    def sync_state(self):

        print(self.sync_paths)

        for path in self.sync_paths:
            resp = requests.post( url = path + '/sync' )
            if resp.status_code != 200:
                raise Exception("Services couldn't be Synchronised. Error: {}".format(resp.text))

        return 0
