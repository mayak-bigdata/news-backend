
from .object import Category, CategoryInterface
from pymongo.database import Database
from typing import List

class NameExists(Exception):
    pass

class CategoryService():

    def __init__(self, db: Database):
        self._db = db
        self.temp = {}
        self.count = 0

    def get_all(self) -> List[Category]:
        return list(self.temp.values())

    def get(self, id: int) -> Category:

        if id not in self.temp:
            raise KeyError

        return self.temp[id]

    def has(self, id: int) -> bool:
        return id in self.temp

    def create(self, new: CategoryInterface) -> Category:

        if new['name'] in map(lambda x: x.name, self.temp.values()):
            raise NameExists

        id = self.count; self.count += 1

        new_obj = Category( id = id, name = new['name'] )
        self.temp[id] = new_obj

        return self.temp[id]
