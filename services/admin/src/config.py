# admin/src/config.py

import os

class Config(object):

    SAVE_PATH = os.environ.get('SAVE_PATH')
    STATE_PATH = os.environ.get('STATE_PATH')

    AUDIO_URL = "http://{}:{}/digests".format(os.environ.get("AUDIOSERVICE_SERVICE_HOST"),
                           os.environ.get("AUDIOSERVICE_SERVICE_PORT"))
    ENV = 'test'
    JSON_AS_ASCII = False
