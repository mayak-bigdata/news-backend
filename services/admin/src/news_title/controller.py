from flask import request, abort
from flask_accepts import accepts, responds
from flask_restx import Namespace, Resource
from flask.wrappers import Response
from typing import List
from injector import inject


from .object import NewsTitle, NewsTitleSchema, NewsTitleInterface
from .service import NewsTitleService, NoCategory

api = Namespace('NewsTitle', description='Responsible for management of news titles')

######
## Text news control
######

@api.route('/')
class NewsTitleResource(Resource):
    '''NewsTitles'''

    @inject
    def __init__(self, service: NewsTitleService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    @accepts(dict(name="num", type=int, help='Number of news you want to get.'), api=api)
    @responds(schema=NewsTitleSchema(many=True), api=api)
    def get(self) -> List[NewsTitle]:
        '''Get last news added to the database'''

        if request.parsed_args['num'] is None:
            abort(400, 'Please state number of titles')

        return self.service.get_num(**request.parsed_args)

    @accepts(schema=NewsTitleSchema, api=api)
    @responds(schema=NewsTitleSchema, api=api)
    def post(self) -> NewsTitle:
        '''Create a Single title'''
        try:
            return self.service.create(request.parsed_obj)
        except NoCategory:
            abort(404, description='Category does not exist.')


@api.route('/<int:titleId>')
@api.param('titleId', 'NewsTitle database ID')
class NewsTitleIdResource(Resource):

    @inject
    def __init__(self, service: NewsTitleService, **kwargs):
        self.service = service
        super().__init__(**kwargs)

    @responds(schema=NewsTitleSchema, api=api)
    def get(self, titleId: int) -> NewsTitle:
        '''Get Single NewsTitle'''

        try:
            return self.service.get(titleId)
        except KeyError:
            abort(404, description='Title was not found.')
