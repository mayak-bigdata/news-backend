from mypy_extensions import TypedDict
from marshmallow import fields, Schema
from dataclasses import dataclass

### Model

@dataclass
class NewsTitle:
    id: int
    name: str
    text: str
    cat: int

### Interface

class NewsTitleInterface(TypedDict, total=False):

    id: int
    name: str
    text: str
    cat: int

### Schema

class NewsTitleSchema(Schema):

    id = fields.Integer(readonly=True, description='The news unique identifier')
    name = fields.String(required=True, description='The news name')
    text = fields.String(required=True, description='Short summary of the article')
    cat = fields.Integer(required=True,
                   description='Category of a title (usually matches the name of the digest)')
