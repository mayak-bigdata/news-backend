# recommender/tests/test-engine.py

import unittest
from functools import reduce
import importlib.util
import numpy as np

spec = importlib.util.spec_from_file_location("src.engine", "../src/engine.py")
engine = importlib.util.module_from_spec(spec)
spec.loader.exec_module(engine)

class TestEngine(unittest.TestCase):
    """
    Test the behaviour of trivial recommendations engine
    """

    def test_empty(self):

        print("### Test empty ###")
        news_by_cat = {
            1: [0,1,2,3,4,5],
            2: [6,7,8,9,10,11,12,13],
            3: [14,15,16,17]
        }

        news = reduce( lambda x, y: x+y, news_by_cat.values() )

        num = 8

        rec = engine.RecEngine( news_by_cat, news, num )
        recommendations = rec.recommend( {} )

        print(recommendations)

        self.assertEqual( recommendations, [0,1,2,3,4,5,6,7] )


    def test_one(self):

        print("### Test one category ###")
        news_by_cat = {
            1: [0,1,2,3,4,5],
            2: [6,7,8,9,10,11,12,13],
            3: [14,15,16,17]
        }
        news = reduce( lambda x, y: x+y, news_by_cat.values() )
        num = 5
        feedback = { 0: {'cat': 2}, 1: {'cat': 2} }

        rec = engine.RecEngine( news_by_cat, news, num )
        recommendations = rec.recommend( feedback )
        print(recommendations)
        self.assertEqual( recommendations, [6,7,8,9,10])

    def test_not_enough(self):
        print("### Not enough scenario ###")
        # Test case when one category is not enough to fill the whole digest.
        news_by_cat = {
            1: [0,1,2,3,4,5],
            2: [6,7,8,9,10,11,12,13],
            3: [14,15,16,17]
        }
        news = reduce( lambda x, y: x+y, news_by_cat.values() )
        num = 7
        feedback = { 0: {'cat':3} }

        rec = engine.RecEngine( news_by_cat, news, num )
        recommendations = rec.recommend( feedback )
        print(recommendations)

        self.assertTrue( np.all(np.isin([14,15,16,17], recommendations)) )
        self.assertEqual( num, np.unique(recommendations).shape[0] )

    def test_mixture(self):
        print("### Test multiple categories ###")
        news_by_cat = {
            1: [0,1,2,3,4,5],
            2: [6,7,8,9,10,11,12,13],
            3: [14,15,16,17]
        }
        news = reduce( lambda x, y: x+y, news_by_cat.values() )
        num = 5
        feedback = { 0: {'cat':2}, 1: {'cat': 2}, 2: {'cat': 1}, 3: {'cat': 3} }

        rec = engine.RecEngine( news_by_cat, news, num )
        recommendations = rec.recommend( feedback )
        print(recommendations)

        self.assertEqual( recommendations, [6,7,8,0,1])


if __name__ == '__main__':
    unittest.main()
