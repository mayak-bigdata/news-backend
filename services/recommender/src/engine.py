from collections import defaultdict
from numpy import round
from math import ceil

class RecEngine:

    def __init__(self, news_by_cat: dict, all_news: list, num_news: int):
        self.news_by_cat = news_by_cat
        self.num_news = num_news
        self.all_news = all_news
        self.category_ids = self.news_by_cat.keys()

    def recommend(self, feedback: dict):

        # Current algorithm just looks at the percentage of news from each
        # category that user has already viewed and then chooses
        # the same percentage of news from cached ones into digest.

        cat_counts = defaultdict(int)

        for f in feedback.keys():
            cat_counts[feedback[f]['cat']] += 1

        total_num = sum([cat_counts[x] for x in self.category_ids])

        recommendations = []

        if total_num == 0: # user doesn't have feedbacks
            recommendations = self.all_news[:self.num_news]

        else:
            for cat in sorted( self.category_ids, key=lambda x: cat_counts[x],
                                     reverse=True ):

                desired_num = int(ceil(self.num_news * cat_counts[cat] / total_num))
                total_space = self.num_news - len(recommendations)

                if total_space == 0:
                    break

                if desired_num == 0:
                    desired_num = len(self.news_by_cat[cat])

                final_num = min( desired_num, total_space )

                recommendations.extend( self.news_by_cat[cat][:final_num] )

        return recommendations
