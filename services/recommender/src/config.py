# recommender/src/config.py

import os

class Config(object):

    MONGO_HOST = os.environ.get('MONGO_HOST')
    MONGO_PORT = os.environ.get('MONGO_PORT')
    MONGO_DB = os.environ.get('MONGO_DB')
    MONGO_ARGS = os.environ.get('MONGO_ARGS')
    LOCAL_MONGO = int(os.environ.get('LOCAL_MONGO'))

    DB_USER = os.environ.get('DB_USER')
    DB_PASS = os.environ.get('DB_PASS')

    LOGIN_IP   = os.environ.get('REGISTRATION_SERVICE_HOST')
    LOGIN_PORT = os.environ.get('REGISTRATION_SERVICE_PORT')

    # Number of news in one digest
    NUMBER_OF_NEWS = int(os.environ.get('NUMBER_OF_NEWS'))

    JSON_AS_ASCII = False
