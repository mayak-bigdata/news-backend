# recommender/src/views.py

from flask import Blueprint, request, make_response, jsonify, redirect
from flask.views import MethodView
from src import db, app, recommender
import requests
import numpy as np

recommender_blueprint = Blueprint('recommender', __name__)

## !!!!!!!!!!!!!!!!
## While we don't have event driven architecture implemented - this
## service can't be scaled to more then one copy!
## This is due to state management issues (any way I can come fucks up performance)
## Right now recommendations for users are stored in the (balls) local pod cache,

#########
# Health check
#########

class HealthCheck(MethodView):

    def get(self):
        responseObject = {
            'message': 'ok'
        }
        return make_response(jsonify(responseObject)), 200


#########
# Feedback
#########

class FeedbackAPI(MethodView):

    def post(self):

        try:
            # Authorization
            headers = request.headers
            login_path = 'http://' + app.config.get('LOGIN_IP') + ':' + app.config.get('LOGIN_PORT') + '/registration/info'
            resp = requests.get( url = login_path, headers = headers )

            # Saving feedback
            if resp.status_code == 200:

                user_id = resp.json()["data"]["user_id"]
                react = request.get_json().get('reactions')
                react = { int(x):int(react[x]) for x in react.keys() } # not sure why it returns strings

                db.add_reactions(user_id, react)

                responseObject = {
                    'message': 'ok'
                }
                return make_response(jsonify(responseObject)), 200

            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Please authorize.'
                }
                return make_response(jsonify(responseObject)), 401

        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Something went wrong.',
                'error': e
            }
            return make_response(jsonify(responseObject)), 500

    def get(self):
        res = list( db.reactions.find({}))
        for x in res:
            x.pop('_id', None)
        return jsonify( res )

#########
# Recommendations
#########

class RecommendationsAPI(MethodView):

    def get(self):

        try:

            # Authorization
            headers = request.headers
            login_path = 'http://' + app.config.get('LOGIN_IP') + ':' + app.config.get('LOGIN_PORT') + '/registration/info'
            resp = requests.get( url = login_path, headers = headers )

            if resp.status_code == 200:

                user_id = resp.json()["data"]["user_id"]

                if user_id in db.recommendations:

                    rec = db.recommendations[user_id]

                else:
                    react = db.get_reactions(user_id)
                    rec = recommender.recommend(react)

                    db.recommendations[user_id] = rec # caching

                responseObject = {
                    'status': 'success',
                    'user_id': user_id,
                    'rec': rec
                }

                return make_response(jsonify(responseObject)), 200
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Please authorize.'
                }
                return make_response(jsonify(responseObject)), 401

        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Something went wrong.',
                'error': e
            }
            return make_response(jsonify(responseObject)), 500

#########
# Infrastructure stuff
#########

# functions

health_view = HealthCheck.as_view('health_api')
feedback_view = FeedbackAPI.as_view('feedback_api')
recommend_view = RecommendationsAPI.as_view('recommend_view')

# making handlers

recommender_blueprint.add_url_rule(
    '/',
    view_func=health_view,
    methods=['GET']
)

recommender_blueprint.add_url_rule(
    '/recommender/feedback',
    view_func=feedback_view,
    methods=['GET', 'POST']
)

recommender_blueprint.add_url_rule(
    '/recommender/recommend',
    view_func=recommend_view,
    methods=['GET']
)
