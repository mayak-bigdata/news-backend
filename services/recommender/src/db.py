import json
import pymongo
import requests
import os
from collections import defaultdict

# Category 0 is for personal digests

class DB:

    def __init__(self, config):

        # Connecting to mongodb for reactions

        if config.LOCAL_MONGO:
            url = "mongodb://{}:{}@{}:{}/{}?{}".format(
                config.DB_USER,
                config.DB_PASS,
                config.MONGO_HOST,
                config.MONGO_PORT,
                config.MONGO_DB,
                config.MONGO_ARGS
            )
        else:
            url = "mongodb+srv://{}:{}@{}/{}?{}".format(
                config.DB_USER,
                config.DB_PASS,
                config.MONGO_HOST,
                config.MONGO_DB,
                config.MONGO_ARGS
            )

        client = pymongo.MongoClient(url)
        db = client[config.MONGO_DB]
        self.reactions = db['reactions']

        # Synchronizing, which news are currently available

        news_collection = db['news']

        resp = requests.get( url = "http://{}:{}/digests/sync".format(os.environ.get("AUDIOSERVICE_SERVICE_HOST"),
                               os.environ.get("AUDIOSERVICE_SERVICE_PORT")) )

        if resp.status_code != 200:
            raise Exception(str(resp.status_code))

        ids = resp.json()['contents']

        found_news = news_collection.find({"id": {"$in": ids}})

        self.cached_news = { x['id'] : x['cat'] for x in found_news }
        ## Storage is algo specific here - might want to change it later.

        self.news_by_cat = defaultdict(list)

        for x in self.cached_news.keys():
            self.news_by_cat[ self.cached_news[x] ].append( x )

        # Cache for recommendations
        # TODO: Switch it to normal cache (like LRU)

        self.recommendations = {}

    def add_reactions( self, user_id, reactions ):

        prep_reac = {'id_'+str(x):
            { 'react': reactions[x], 'cat': self.cached_news[x] }
            for x in reactions.keys()}

        return self.reactions.update_one( {'user_id': user_id},
                                           {'$set': prep_reac}, upsert=True)

    def get_reactions(self, user_id):

        res = list(self.reactions.find( {"user_id" : user_id} ))

        if len(res) == 0:
            return {}
        elif len(res) != 1:
            raise Exception('Multiple users found')
        else:
            target = res[0]
            news_keys = filter(lambda x: x.startswith('id'), target.keys())
            return {key.strip('id_') : target[key] for key in news_keys}
