# recommender/src/__init__.py

import os
from flask import Flask
from .config import Config
from .db import DB
from .engine import RecEngine

app = Flask(__name__)
app.config.from_object(Config)

conf = Config()
db = DB(conf)

recommender = RecEngine(db.news_by_cat, list(db.cached_news.keys()), conf.NUMBER_OF_NEWS)

from .views import recommender_blueprint
app.register_blueprint(recommender_blueprint)
