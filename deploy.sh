#!/usr/bin/fish

##################
##### Building
##################

function standard_build
  echo "Building and deploying $argv[2] on test server..."
  eval (minikube docker-env)
  cd services/$argv[2]
  docker build -f ./deployment/Dockerfile -t $argv[2]:latest .
  kubectl scale deployment $argv[2] --replicas=0 -n default
  kubectl scale deployment $argv[2] --replicas=1 -n default
end

if test $argv[1] = 'build'

  if test $argv[2] = 'audioservice'
    standard_build $argv
  end

  if test $argv[2] = 'recommender'
    standard_build $argv
  end

  if test $argv[2] = 'registration'
    standard_build $argv
  end

###################
####### Creating
###################

function standard_create
  echo "Creating $argv[2]..."
  eval (minikube docker-env)
  cd services/$argv[2]
  docker build -f ./deployment/Dockerfile -t $argv[2]:latest .
  cd ../..
  kubectl create -f ./kubernetes/$argv[2]/$argv[2]-service.yml
  kubectl create -f ./kubernetes/$argv[2]/$argv[2]-deployment.yml
end

else if test $argv[1] = 'create'

  if test $argv[2] = 'audioservice'
    standard_create $argv
  end

  if test $argv[2] = 'recommender'
    standard_create $argv
  end

  if test $argv[2] = 'registration'
    standard_create $argv
  end
end
##################
####### Patching
##################

function standard_patch
  echo "Updating $argv[2] yaml..."
  eval (minikube docker-env)
  kubectl delete svc $argv[2]
  kubectl delete deploy $argv[2]
  kubectl create -f ./kubernetes/$argv[2]/$argv[2]-service.yml
  kubectl create -f ./kubernetes/$argv[2]/$argv[2]-deployment.yml
end

if test $argv[1] = 'patch'

  if test $argv[2] = 'ingress'
    echo 'Updating ingress...'
    kubectl apply -f ./kubernetes/ingress.yml
  end

  if test $argv[2] = 'register-config'
    echo 'Updating register-config...'
    kubectl apply -f ./kubernetes/registration/register-config.yml
    kubectl apply -f ./kubernetes/registration/registration-secret.yml
  end

  if test $argv[2] = 'audioservice'
    kubectl apply -f ./kubernetes/audioservice/audioservice-config.yml
    standard_patch $argv
  end

  if test $argv[2] = 'registration'
    standard_patch $argv
  end

end

##################
####### Localize
##################

if test $argv[2] = 'registration'
  set ip 5060
end

if test $argv[2] = 'audioservice'
  set ip 8080
end

function localize
  cd services/$argv[2]
  telepresence --expose $ip --swap-deployment $argv[2] \
          --docker-run --rm -it -v (pwd):/usr/src/app $argv[2]-dev
end

if test $argv[1] = 'localize'
  localize $argv
end

##################
####### Build development image
##################

function build_dev
  cd services/$argv[2]
  docker build . -t $argv[2]-dev:latest -f deployment/Dockerfile
end

if test $argv[1] = 'build_dev'
  build_dev $argv
end
